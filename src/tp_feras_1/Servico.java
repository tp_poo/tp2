/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_feras_1;

/**
 *
 * @author andre
 */
public class Servico {
    private float preco;
    protected int tipo;
    private String descricao;
    
    
    public Servico(float preco, int tipo, String descricao){
        this.preco = preco;
        this.tipo = tipo;
        this.descricao = descricao;
    }

    public Servico() {
        
    }

    @Override
    public String toString() {
        return preco + "\n" + tipo + "\n" + descricao +'\n';
    }
    
    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    
}
